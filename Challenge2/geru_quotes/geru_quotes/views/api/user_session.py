from cornice import Service
from pyramid.httpexceptions import HTTPOk

from geru_quotes.models.user_session import UserSession
from geru_quotes.views.api.schema import UserSessionSchema

user_session_by_id = Service(name='user_session', path='api/session/{id}')


@user_session_by_id.get(
    content_type='application/json'
)
def show_user_session_by_id(request):
    data_db = request.dbsession.query(UserSession).filter_by(id=request.matchdict['id']).one()

    schema = UserSessionSchema()
    json_data = schema.dump(data_db)

    return HTTPOk(json=json_data)


user_session_all = Service(name='user_sessions', path='api/sessions')


@user_session_all.get(
    content_type='application/json'
)
def show_user_session_all(request):
    data_db = request.dbsession.query(UserSession).all()
    schema = UserSessionSchema(many=True)
    json_data = schema.dump(data_db)

    return HTTPOk(json=dict(sessions=json_data))
